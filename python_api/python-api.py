from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify

app = Flask(__name__)
api = Api(app)

class Info(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Already started'} # Fetches first column that is Employee ID

class Info2(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Running'} # New feature issue https://gitlab.com/malzew/netology-python/-/issues/1
class Info3(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Running ololo'} # New feature issue https://gitlab.com/malzew/netology-python/-/issues/1

api.add_resource(Info, '/get_info') # Route_1
api.add_resource(Info2, '/rest/api/get_info') # # New feature issue https://gitlab.com/malzew/netology-python/-/issues/1
api.add_resource(Info3, '/rest/api/get_info1') # # New feature issue https://gitlab.com/malzew/netology-python/-/issues/3"


if __name__ == '__main__':
     app.run(host='0.0.0.0', port='5290')