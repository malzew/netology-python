FROM registry.gitlab.com/malzew/netology-python/centos7-python:3.7

RUN groupadd --gid 1000 service \
    && useradd --uid 1000 --gid 1000 --home-dir /python_api service

USER service

WORKDIR /python_api

COPY ./python_api/* /python_api/
COPY ./requirements.txt /python_api/requirements.txt

ENV PATH=$PATH:/python_api/.local/bin

RUN python3.7 -m pip install --upgrade pip && \
    pip3.7 install --upgrade setuptools && \
    pip3.7 install -r requirements.txt

EXPOSE 5290

CMD ["python3.7", "/python_api/python-api.py"]
